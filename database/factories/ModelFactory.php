<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define( App\User::class, function ( Faker\Generator $faker ) {
	static $password;

	return [
		'name'           => $faker->name,
		'email'          => $faker->unique()->safeEmail,
		'password'       => $password ?: $password = bcrypt( 'secret' ),
		'remember_token' => str_random( 10 ),
	];
} );

// $threads = factory('App\Thread', 50)->create()
$factory->define( App\Thread::class, function ( $faker ) {
	return [
		'user_id' => function () {
			return factory( 'App\User' )->create()->id;
		},
		'title'   => $faker->sentence,
		'body'    => $faker->paragraph,
	];
} );

// $threads->each(function ($thread) {factory('App\Reply', 10)->create(['thread_id'=> $thread->id]);});
$factory->define( App\Reply::class, function ( $faker ) {
	return [
		'thread_id' => function () {
			return factory( 'App\User' )->create()->id;
		},
		'user_id'   => function () {
			return factory( 'App\User' )->create()->id;
		},
		'body'      => $faker->paragraph,
	];
} );