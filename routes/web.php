<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/threads', 'ThreadsController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
