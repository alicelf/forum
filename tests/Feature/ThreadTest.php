<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ThreadTest extends TestCase {

	use DatabaseMigrations;

	public function test_a_user_can_browse_threads() {
		$response = $this->get( '/threads' );
//    $thread = factory('App\Thread')->create();
//		$response->assertSee( $thread->title );
		$response->assertStatus( 200 );
	}
}
